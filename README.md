# Digital Twin as a Service

This is a sample repository to demonstrate the use of Gitlab
CI/CD pipeline features inside Digital Twin as a Service (DTaaS).

This feature of DTaaS requires a functioning and live
Gitlab runner linked to this gitlab project.
The [BUILD](./BUILD.md) demonstrates the verification
of a working runner linked to this project.

## Digital Twins

The `digital_twins` directory contains two example Digital Twins (DTs).
These DTs can be used as inspiration to build new DTs for your
local DTaaS installation.
